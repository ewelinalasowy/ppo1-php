<?php

use PPO\Zoo\Animals\Elephant;
use PPO\Zoo\Animals\Lion;
use PPO\Zoo\Animals\Chimpanzee;
use PPO\Zoo\Animals\Dolphin;
use PPO\Zoo\Animals\Penguin;
use PPO\Zoo\Animals\Porcupine;
use PPO\Zoo\Animals\Tiger;

use PPO\Zoo\Zoo;
use PPO\Zoo\Food;
use PPO\Zoo\Log;

use Carbon\Carbon;

require "./vendor/autoload.php";

$zoo = new Zoo("Zoo Legnica");

$zoo->addAnimal(new Lion("Simba"))
    ->addAnimal(new Lion("Mufasa"))
    ->addAnimal(new Elephant("Dumbo"))
    ->addAnimal(new Chimpanzee("Tytus"))
    ->addAnimal(new Dolphin("Meg"))
    ->addAnimal(new Penguin ("Kowalski"))
    ->addAnimal(new Porcupine("Porcupine"))
    ->addAnimal(new Tiger("Shere Khan"));


$date = Carbon::createFromTime(8, 0, 0);

for ($i = 0; $i < 1; $i++) {
    switch ($i) {

        case 0:
            {
                Log::info();
                Log::info("Time: " . $date);
                $zoo->feedAnimals(food::water());
                $date->addMinutes(60);

            }

        case 1:
            {
                Log::info();
                Log::info("Time: ". $date);
                $zoo->feedAnimals(food::vegetables());
                $date->addMinutes(60);

            }

        case 2:
            {
                Log::info();
                Log::info("Time: ". $date);
                $zoo->feedAnimals(food::fruits());
                $date->addMinutes(60);

            }
        case 3:
            {
                Log::info();
                Log::info("Time: ". $date);
                $zoo->feedAnimals(food::leaves());
                $date->addMinutes(60);

            }
        case 4:
            {
                Log::info();
                Log::info("Time: ". $date);
                $zoo->feedAnimals(food::meat());
                $date->addMinutes(60);

            }
        case 5:
            {
                Log::info();
                Log::info("Time: ". $date);
                $zoo->feedAnimals(food::fish());
                $date->addMinutes(60);

            }

            default: {}

    }
}



