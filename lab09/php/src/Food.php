<?php


namespace PPO\Zoo;


class Food
{
    public static function fruits(): String
    {
        return "fruits";
    }

    public static function vegetables(): String
    {
        return "vegetables";
    }

    public static function leaves(): String
    {
        return "leaves";
    }

    public static function meat():string {
        return "meat";
    }

    public static function fish():string{
        return "fish";
    }

    public static function water():string {
        return "water";
}

}