<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 18.12.2018
 * Time: 14:55
 */

namespace PPO\Zoo\Animals;


abstract class Carnivorous extends Animal
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }

protected function getDiet():array
    {
        return ["meat","fish","water"];
    }

}