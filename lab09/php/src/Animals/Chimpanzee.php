<?php

namespace PPO\Zoo\Animals;


class Chimpanzee extends Omnivorous
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }
}