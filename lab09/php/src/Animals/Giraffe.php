<?php


namespace PPO\Zoo\Animals;


class Giraffe extends Herbivorous
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }
}