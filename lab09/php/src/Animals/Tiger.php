<?php


namespace PPO\Zoo\Animals;


class Tiger extends Carnivorous
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }
}