<?php


namespace PPO\Zoo\Animals;


class Penguin extends Carnivorous
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }
}