<?php


namespace PPO\Zoo\Animals;


abstract class Omnivorous extends Animal
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }

    protected function getDiet(): array
    {
        return ["vegetables", "fruits", "leaves", "meat", "fish", "water"];
    }
}