<?php

namespace PPO\Zoo\Animals;


class Dolphin extends Carnivorous
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }
}