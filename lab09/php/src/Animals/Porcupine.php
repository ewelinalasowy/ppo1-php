<?php


namespace PPO\Zoo\Animals;


class Porcupine extends Herbivorous
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }
}