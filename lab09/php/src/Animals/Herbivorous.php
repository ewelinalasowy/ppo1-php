<?php

namespace PPO\Zoo\Animals;


abstract class Herbivorous extends Animal
{
    public function __construct(string $name)
    {
        parent::__construct($name);
    }

protected function getDiet(): array
    {
        return ["vegetables", "fruits", "leaves", "water"];
    }
}