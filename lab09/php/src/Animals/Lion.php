<?php

namespace PPO\Zoo\Animals;

class Lion extends Carnivorous {

	public function __construct(string $name)
    {
        parent::__construct($name);
    }

}
