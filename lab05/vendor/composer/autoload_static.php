<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitf8f42555b84caf3bb910efe5d5361ed9
{
    public static $prefixLengthsPsr4 = array (
        'L' => 
        array (
            'Library\\' => 8,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Library\\' => 
        array (
            0 => __DIR__ . '/../..' . '/Library',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitf8f42555b84caf3bb910efe5d5361ed9::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitf8f42555b84caf3bb910efe5d5361ed9::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
