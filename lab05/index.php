<?php

require "./vendor/autoload.php";

use Library\Library;

$library = new Library();
$library->run();
