<?php

namespace Library;

use Library\Books\ComicBook;
use Library\Books\Novel;
use Library\Books\Textbook;


final class Library
{

    private $novels = [];
    private $comicbooks =[];
    private $textbooks =[];

    public function addNovel(Novel $novel):self
    {
        $this->novels[] = $novel;
        return $this;
    }

    public function addComicBook(ComicBook $comicbook): self
    {
        $this->comicbooks[] = $comicbook;
        return $this;
    }

    public function addTextbook(Textbook $textbook): self
    {
        $this->textbooks[] = $textbook;
        return $this;
    }

    public function getNovels(): array
    {
            return $this->novels;
    }

    public function getComicBooks(): array
    {
            return $this->comicbooks;
    }

    public function getTextBooks(): array
    {
            return $this->textbooks;
    }

    public function writeNovels(): void
    {
        foreach ($this->getNovels() as $book) {
            echo $book->getString();
            echo PHP_EOL;
        }
    }

    public function writeComicBooks(): void
    {
        foreach ($this->getComicBooks() as $book) {
            echo $book->getString();
            echo PHP_EOL;
        }
    }

    public function writeTextBooks(): void
    {
        foreach ($this->getTextBooks() as $book) {
            echo $book->getString();
            echo PHP_EOL;
        }
    }

    public function writeAllBooks(): void
    {
        $this->writeNovels();
        $this->writeComicBooks();
        $this->writeTextBooks();
    }

    public function saveBook(int $param): void
    {
        $title = readline("Title of book: ");
        $authors = readline("Authors: ");
        $numberOfPages = readline("Number of pages: ");
        $publishingHouse = readline("Publishing house: ");

        if ($param == 1) {
            $typeofNovel = readline("Type of novel: ");
            $this->addNovel(new Novel($title, $authors, $numberOfPages, $publishingHouse, $typeofNovel));
        }

        if ($param == 2) {
            $designer = readline("Designer: ");
            $numberOfTom = readline("Number of tom: ");
            $this->addComicBook(new ComicBook($title, $authors, $numberOfPages, $publishingHouse, $designer, $numberOfTom));
        }

        if ($param == 3) {
            $yearOfAdmission = readline("Year of admission: ");
            $numberOfAdmission = readline("Number of admission: ");
            $this->addTextbook(new Textbook($title, $authors, $numberOfPages, $publishingHouse, $yearOfAdmission, $numberOfAdmission));
        }
    }


    public function menu(): void
    {
        Log::info("1 - add novel");
        Log::info("2 - add comicbook");
        Log::info("3 - add textbook");
        Log::info("4 - show all novels");
        Log::info("5 - show all comicbooks");
        Log::info("6 - show all textbooks");
        Log::info("7 - show all books");
    }

    public function run(): void
    {
        do {
            $this->menu();
            $options = readline("Specify option:");

            switch ($options) {
                case 1:
                    {
                        $this->saveBook(1);
                        break;
                    }
                case 2:
                    {
                        $this->saveBook(2);
                        break;
                    }
                case 3:
                    {
                        $this->saveBook(3);
                        break;
                    }
                case 4:
                    {
                            $this->writeNovels();

                        break;
                    }
                case 5:
                    {
                        $this->writeComicBooks();

                        break;
                    }
                case 6:
                    {
                        $this->writeTextBooks();
                        break;
                    }
                case 7:
                    {
                        $this->writeAllBooks();
                        break;
                    }
            }


        } while ($options >= 1 && $options <= 7);
    }
}

