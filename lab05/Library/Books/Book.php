<?php

namespace Library\Books;

abstract class Book
{

    protected $title;
    protected $authors;
    protected $numberOfPages;
    protected $publishingHouse;

    public function __construct(string $title, string $authors, int $numberOfPages, string $publishingHouse)
    {
        $this->title = $title;
        $this->authors = $authors;
        $this->numberOfPages = $numberOfPages;
        $this->publishingHouse = $publishingHouse;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAuthors(): string
    {
        return $this->authors;
    }

    public function getNumberOfPages(): int
    {
        return $this->numberOfPages;
    }

    public function getPublishingHouse(): string
    {
        return $this->publishingHouse;
    }

    abstract public function getType(): string;

}
