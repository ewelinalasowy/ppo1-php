<?php

namespace Library\Books;

class Textbook extends Book
{

    private $yearOfAdmission;
    private $numberOfAdmission;


    public function __construct(string $title, string $authors, int $numberOfPages, string $publishingHouse, int $yearOfAdmission, string $numberOfAdmission)
    {
        parent::__construct($title, $authors, $numberOfPages, $publishingHouse);
        $this->yearOfAdmission = $yearOfAdmission;
        $this->numberOfAdmission = $numberOfAdmission;
    }

    public function getYearOfAdmission(): int
    {
        return $this->yearOfAdmission;
    }

    public function getNumberOfAdmission(): string
    {
        return $this->numberOfAdmission;
    }

    public function getType(): string
    {
        return "textbook";
    }

    public function getString(): string
    {
        $sep = " || ";
        return $this->getTitle() . $sep . $this->getAuthors() . $sep . $this->getNumberOfPages() . $sep . $this->getPublishingHouse() . $sep . $this->getYearOfAdmission() . $sep . $this->getYearOfAdmission();
    }
}
