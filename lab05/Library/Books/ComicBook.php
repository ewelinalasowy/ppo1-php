<?php

namespace Library\Books;

class ComicBook extends Book
{

    private $designer;
    private $numberOfTom;

    public function __construct(string $title, string $authors, int $numberOfPages, string $publishingHouse, string $designer, int $numberOfTom)
    {
        parent::__construct($title, $authors, $numberOfPages, $publishingHouse);
        $this->designer = $designer;
        $this->numberOfTom = $numberOfTom;
    }

    public function getDesigner(): string
    {
        return $this->designer;
    }

    public function getNumberOfTom(): int
    {
        return $this->numberOfTom;
    }

    public function getType(): string
    {
        return "comic book";
    }

    public function getString(): string
    {
        $sep = " || ";
        return $this->getTitle() . $sep . $this->getAuthors() . $sep . $this->getNumberOfPages() . $sep . $this->getPublishingHouse() . $sep . $this->getDesigner() . $sep . $this->getNumberOfTom();
    }


}

