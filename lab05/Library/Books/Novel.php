<?php

namespace Library\Books;

class Novel extends Book
{

    private $typeOfNovel;

    public function __construct(string $title, string $authors, int $numberOfPages, string $publishingHouse, string $typeOfNovel)
    {
        parent::__construct($title, $authors, $numberOfPages, $publishingHouse);
        $this->typeOfNovel = $typeOfNovel;
    }

    public function getTypeOfNovel(): string
    {
        return $this->typeOfNovel;
    }

    public function getType(): string
    {
        return "novel";
    }

    public function getString(): string
    {
        $sep = " || ";
        return $this->getTitle() . $sep . $this->getAuthors() . $sep . $this->getNumberOfPages() . $sep . $this->getPublishingHouse() . $sep . $this->getTypeOfNovel();
    }
}