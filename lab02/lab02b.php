<?php

const STUDENTS_COUNT = 10;

class Student {

	public $studentNo;
	public $studentName;
	public $studentSurname;
	public $studentActivity;

	public function setStudentNo(string $studentNo): void {
		$this->studentNo = $studentNo;
	}
		
	public function getStudentNo(): string {
		return $this->studentNo;
	}

    public function setStudentName(string $studentName): void {
        $this->studentName = $studentName;
    }

    public function getStudentName(): string {
        return $this->studentName;
    }
    public function setStudentSurname(string $studentSurname): void {
        $this->studentSurname = $studentSurname;
    }

    public function getStudentSurname(): string {
        return $this->studentSurname;
    }

    public function setStudentActivity(int $studentActivity): void {
        $this->studentActivity = $studentActivity;
    }

    public function getStudentActivity(): string {
        return $this->studentActivity;
    }


}

function getRandomStudentNumber(): string {
	$randomStudentNumber = rand() % 2000 + 38000;
	return (string) $randomStudentNumber;
}

function getRandomStudentName(): string{

    $names=["1","2","3","4","5","6","7","8","9","10"];
    $randomIndex=rand()%10;
    return (string) $names[$randomIndex];;
}

function getRandomStudentSurname():string {

    $surnames=["a","b","c","d","e","f","g","h","i","j"];
    $randomIndex=rand()%10;
    return (string) $surnames[$randomIndex];
}

function getRandomStudentActivity():int{

    return rand()%2;
}


$students = [];

for($i = 0; $i < STUDENTS_COUNT; $i++) {
	$student = new Student();
	$student->setStudentNo(getRandomStudentNumber());
	$student->setStudentName(getRandomStudentName());
	$student->setStudentSurname(getRandomStudentSurname());
	$student->setStudentActivity(getRandomStudentActivity());
	$students[] = $student;
}

echo "Students group have been filled." . PHP_EOL . PHP_EOL;

for($i = 0; $i < sizeof($students); $i++) {
	$student = $students[$i];
	if ($student->getStudentActivity() ==1)
    {
	echo $student->getStudentNo() ." ";
	echo $student->getStudentName() . " ";
	echo $student->getStudentSurname() . PHP_EOL;
    }

}

