<?php

class Point {

	public $x, $y;

	public function __construct(float $x, float $y) {
		$this->x = $x;
		$this->y = $y;
		echo "Point [" . $this->x . ", " . $this->y . "] has been created." .PHP_EOL;
	}

	public function __destruct() {
		echo "Point [" . $this->x . ", " . $this->y . "] has been deleted." . PHP_EOL;
	}

	public function movePoint(float $xAxisShift, float $yAxisShift): void {
		$this->x += $xAxisShift;
		$this->y += $yAxisShift;
	}

}

class Circle {

	public $center;
	public $radius;
	
	public function __construct(Point $center, float $radius) {
		$this->center = $center;
		$this->radius = $radius;
	}

	public function getCoordinates(): void {
		echo "x: " . $this->center->x . PHP_EOL;
		echo "y: " . $this->center->y . PHP_EOL;
	}

};


class Square {

    public $leftdown;
    public $leftup;
    public $rightdown;
    public $rightup;
    public $side;

    public function __construct(Point $rightup, int $side)
    {
        $this->rightup = $rightup;
        $this->side=$side;
        $this->makeSquare();
    }

    public function __destruct()
    {
        echo "Square has been deleted" . PHP_EOL;
    }


    public function makeSquare():void
    {
        $this->leftup = new Point ($this->rightup->x-$this->side, $this->rightup->y);
        $this->leftdown = new Point ($this->rightup->x-($this->side), $this->rightup->y-($this->side));
        $this->rightdown = new Point ($this->rightup->x, $this->rightup->y-($this->side));
    }


    public function getCoordinates():void
    {
        echo "Right up: " . $this->rightup->x .  $this->rightup->y . PHP_EOL;
        echo "Right down: " . $this->rightdown->x .  $this->rightdown->y . PHP_EOL;
        echo "Right up: " . $this->leftdown->x .  $this->leftdown->y . PHP_EOL;
        echo "Right up: " . $this->leftup->x .  $this->leftup->y . PHP_EOL;

    }


}



$rightup = new Point (rand(0,10),rand(0,10));
$side = rand()%10+1;
$square = new Square($rightup, $side);



