<?php

namespace ParkingLotManager\Entities;

use ParkingLotManager\Interfaces\EntityInterface;

class Bicycle implements EntityInterface {

	public function identify(): string {
		return "Unknown bicycle";
	}

	public function canEnter(): bool {
		return true;
	}

    public function payment(): int{
        return 0;
    }
    public function ifTakeUpSpace(): bool
    {
        return true;
    }
}
