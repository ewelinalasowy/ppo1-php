<?php

namespace ParkingLotManager\Entities;

class TeacherCar extends Car {


    public function identify(): string {
        return "Teacher car with plate number " . $this->plate;
    }

    public function payment(): int{
        return 0;
    }

}
