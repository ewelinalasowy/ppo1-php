<?php



namespace ParkingLotManager\Entities;

class Courier extends Car {


    public function identify(): string {
        return "Courier with plate number " . $this->plate;
    }

    public function payment(): int{
        return 0;
    }

    public function ifTakeUpSpace(): bool
    {
        return false;
    }

}