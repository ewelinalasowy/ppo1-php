<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 12.12.2018
 * Time: 11:58
 */

namespace ParkingLotManager\Entities;
use ParkingLotManager\Interfaces\EntityInterface;


class Tank implements EntityInterface
{

    public function identify(): string {
        return "Unknown tank";
    }

    public function canEnter(): bool {
        return false;
    }

    public function payment(): int{
        return 0;
    }
    public function ifTakeUpSpace(): bool
    {
        return false;
    }

}