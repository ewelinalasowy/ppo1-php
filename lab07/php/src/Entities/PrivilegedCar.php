<?php


namespace ParkingLotManager\Entities;

class PrivilegedCar extends Car {


    public function identify(): string {
        return "Privileged car with plate number " . $this->plate;
    }

    public function payment(): int{
        return 0;
    }

    public function ifTakeUpSpace(): bool
    {
        return false;
    }

}