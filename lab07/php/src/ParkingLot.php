<?php

namespace ParkingLotManager;


use ParkingLotManager\Entities\Car;
use ParkingLotManager\Entities\Bicycle;
use ParkingLotManager\Entities\Pedestrian;
use ParkingLotManager\Entities\Courier;
use ParkingLotManager\Entities\PrivilegedCar;
use ParkingLotManager\Interfaces\EntityInterface;



final class ParkingLot
{
    const DEFAULT_PARKING_SLOTS = 200;
    const DEFAULT_BICYCLES_SLOTS = 30;
    protected $entitiesOnProperty = [];
    protected $carsOnProperty = 0;
    protected $bicyclesOnProperty=0;
    protected $day_earnings = 0;
    protected $numberOfParkingSlots = self::DEFAULT_PARKING_SLOTS;
    protected $numberOfBicyclesSlots = self::DEFAULT_BICYCLES_SLOTS;


    public function checkIfCanEnter(EntityInterface $entity): bool
    {
        return $entity->canEnter();
    }

    public function letIn(EntityInterface $entity): void
    {
        $this->entitiesOnProperty[] = $entity;


        if ($entity instanceof Car && $entity->ifTakeUpSpace() == true && $this->carsOnProperty< $this->numberOfParkingSlots) {
            $this->carsOnProperty++;
            $this->day_earnings += $entity->payment();
            self::let($entity);

        }
        else if($entity instanceof Bicycle  && $this->bicyclesOnProperty< $this->numberOfBicyclesSlots)
        {
            $this->bicyclesOnProperty++;
            self::let($entity);
        }
        else if ($entity instanceof Pedestrian || $entity instanceof Courier || $entity instanceof PrivilegedCar)
        {
            self::let($entity);
        }

        else
        {
            self::notLet($entity);
        }

    }

    public function countCars(): int
    {
        return $this->carsOnProperty;
    }

    public function countDayEarnings(): int
    {
        return $this->day_earnings;
    }


    public function let(EntityInterface $entity):void
    {
        Log::info($entity->identify() . " let in.");
    }

    public function notLet(EntityInterface $entity):void
    {
        Log::info($entity->identify() . " not let in.");
    }
}
