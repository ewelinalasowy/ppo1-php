<?php

namespace ParkingLotManager\Interfaces;

interface EntityInterface {

	public function identify(): string;
	public function canEnter(): bool;
	public function payment():int;
	public function ifTakeUpSpace():bool;

}
