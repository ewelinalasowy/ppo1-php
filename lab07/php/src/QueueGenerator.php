<?php

namespace ParkingLotManager;

use ParkingLotManager\Entities\Car;
use ParkingLotManager\Entities\Bicycle;
use ParkingLotManager\Entities\Pedestrian;
use ParkingLotManager\Entities\PrivilegedCar;
use ParkingLotManager\Entities\TeacherCar;
use ParkingLotManager\Entities\Tank;
use ParkingLotManager\Entities\Courier;


final class QueueGenerator {

	public static function generate(): array {

	    $ANONYMOUS_PEDESTRIANS_COUNT=self::getRandomNumber(50);
        $PEDESTRIANS_COUNT =self::getRandomNumber(5);
        $CARS_COUNT = self::getRandomNumber(30);
        $TEACHER_CARS_COUNT =self:: getRandomNumber(10);
        $BICYCLES_COUNT =self:: getRandomNumber(7);
        $TANKS_COUNT =self:: getRandomNumber(1);
        $COURIERS_COUNT=self::getRandomNumber(2);
        $PRIVILEGED_CARS_COUNT=self::getRandomNumber(1);

	    $queue = [];


		for($i = 0; $i <= $ANONYMOUS_PEDESTRIANS_COUNT; $i++) {
			$queue[] = new Pedestrian();
		}

		for($i = 0; $i <= $PEDESTRIANS_COUNT; $i++) {
			$queue[] = new Pedestrian(self::getRandomName());
		}

		for($i = 0; $i <= $CARS_COUNT; $i++) {
			$queue[] = new Car(self::getRandomPlateNumber());
		}

		for($i = 0; $i <= $TEACHER_CARS_COUNT; $i++) {
			$queue[] = new TeacherCar(self::getRandomPlateNumber());
		}

		for($i = 0; $i <= $BICYCLES_COUNT; $i++) {
			$queue[] = new Bicycle();
		}

        for($i = 0; $i <= $TANKS_COUNT; $i++) {
            $queue[] = new Tank();
        }

        for($i = 0; $i <= $COURIERS_COUNT; $i++) {
            $queue[] = new Courier(self::getRandomPlateNumber());
        }

        for($i = 0; $i <= $PRIVILEGED_CARS_COUNT; $i++) {
            $queue[] = new PrivilegedCar(self::getRandomPlateNumber());
        }

        shuffle($queue);
		return $queue;
	}

	private static function getRandomPlateNumber(): string {
		return "DLX " . random_int(10000, 99999);
	}

	private static function getRandomName(): string {
		$names = ["John", "Jack", "James", "George", "Joe", "Jim"];
		return $names[array_rand($names)];
	}

	private static function getRandomNumber(int $n): int
    {
        return random_int(0,$n);
    }
}
