<?php

require "./vendor/autoload.php";

use Carbon\Carbon;
use ParkingLotManager\Log;
use ParkingLotManager\ParkingLot;
use ParkingLotManager\QueueGenerator;

$queue = QueueGenerator::generate();
$parking = new ParkingLot();

$count = $parking->countCars();
Log::info("There's $count cars in the parking lot.");
Log::info();

$date = Carbon::createFromTime(8, 0, 0);

for ($x = 0; $x <7 ; $x++) {
    foreach ($queue as $entity) {
        if ($parking->checkIfCanEnter($entity)) {
            $parking->letIn($entity);
            Log::info("Time of entry: $date");
            Log::info();
            $date->addSeconds(30);

        }
    }
    $date->addMinutes(70);
    Log::info();
    $queue=QueueGenerator::generate();

}

$count = $parking->countCars();
Log::info();
Log::info("There's $count cars in the parking lot.");
Log::info();
$earnings = $parking->countDayEarnings();
Log::info("Day's earnings = $earnings");