<?php

namespace PPO\Dashboard;

use Exception;
use ReflectionClass;
use ReflectionException;


final class Dashboard {


    public $listOfPosts = array();

    public function run(): void {
		try {
			while(true) {
				echo PHP_EOL;
				$method = readline("What would you like to do? ");

				if ($method == "run")
                    echo "Dashboard is already running." . PHP_EOL;
				else
				$this->findMethod($method);
			}
		} catch(Exception $exception) {
			echo $exception->getMessage();
		}
	}

	/**
	 * @param string $method
	 * @throws Exception
	 */
	private function findMethod(string $method): void {
		try {
			$reflection = new ReflectionClass(get_class($this));
			$reflectedMethod = $reflection->getMethod($method);
			$reflectedMethod->setAccessible(true);
			$reflectedMethod->invoke($this);
		} catch(ReflectionException $e) {
			echo "There's no method like $method." . PHP_EOL;
		}
	}


	private function create():void
    {
        $contentOfPost = readline("Enter the content of the post below ");
        $this->listOfPosts[] = new Post($contentOfPost);
    }

    private function list():void
    {
        $generate = new PostGenerator();
        $this->listOfPosts = array_merge($this->listOfPosts, $generate->generate());
        echo "List of posts has been generated." . PHP_EOL;
    }


    private function show():void {

        $postID = readline("Enter ID of post to show content of it ");

        foreach ($this->listOfPosts as $post)
        {
            if ($post->getPostId() == $postID)
            echo $post->getContentOfPost().PHP_EOL;
        }

    }

    private function random():void {

        if (sizeof($this->listOfPosts)<=0)
        {
            echo "List of posts is empty";
        }

        else
        {
            $index = rand(0, sizeof($this->listOfPosts)-1);
            $post = $this->listOfPosts[$index];
            echo $post->getContentOfPost().PHP_EOL;
        }


}

    private function showAll () :void
    {
        foreach ($this->listOfPosts as $post)
        {
            echo $post->getPostId().'. '.$post->getContentOfPost().PHP_EOL;
        }
    }

    private function delete():void
    {
        $whatToRemove= readline("What posts you want to delete? all, last or enter number of post");

        if ($whatToRemove == "all")
        {
            $this->listOfPosts = array();
            echo "List of post is empty now.";
        }

        if ($whatToRemove == "last"){
            array_pop($this->listOfPosts);
            echo "Last post has been removed.";

        }

        foreach ($this->listOfPosts as $post)
        {
            if ($post->getPostId() == $whatToRemove)
                unset($this->listOfPosts[$whatToRemove]);
        }
    }

    /**
     * @throws Exception
     */
    private function quit(): void {
        $this->stop();
    }

    /**
     * @throws Exception
     */
    private function stop(): void {
        throw new Exception("Dashboard stopped.");
    }

}