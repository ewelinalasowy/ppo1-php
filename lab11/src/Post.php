<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 09.01.2019
 * Time: 09:36
 */

namespace PPO\Dashboard;


class Post
{
    private $postId=0;
    private $contentOfPost;
    private static $counter=0;

   public function __construct(String $contentOfPost)
   {
       $this->postId = self::$counter++;
       $this->contentOfPost = $contentOfPost;

   }

   public function getPostId() : int
    {
        return $this->postId;
    }

    public function getContentOfPost() : string
    {
        return $this->contentOfPost;
    }

}