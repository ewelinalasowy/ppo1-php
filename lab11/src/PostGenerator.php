<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 09.01.2019
 * Time: 09:37
 */

namespace PPO\Dashboard;


class PostGenerator
{

    const POSTS_COUNT = 10;

    public function generate(): array {
        $listOfPosts = array();
        for ($i=0; $i < self::POSTS_COUNT; $i++)
            $listOfPosts[] = new Post($this->RandomStringGenerator(256));

        return $listOfPosts;
    }


    private function RandomStringGenerator($n) // usunięte static
    {
        $generated_string = "";
        $domain = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        $len = strlen($domain);

        for ($i = 0; $i < $n; $i++)
        {
            $index = rand(0, $len - 1);
            $generated_string = $generated_string . $domain[$index];
        }

        return $generated_string;
    }




}