<?php

class WinnerWasCalled extends Exception
{
}

class Log
{

    static function info(string $message = ""): void
    {
        echo $message . PHP_EOL;
    }

}

class Dice
{

    private $sides;

    public function __construct(int $sides)
    {
        $this->sides = $sides;
    }

    public function getNumberOfSides(): int
    {
        return $this->sides;
    }

    function roll(): int
    {
        $result = rand(1, $this->getNumberOfSides());
        Log::info("Dice roll: " . $result);
        return $result;
    }

}

class Pawn
{

    private $position;
    private $name;

    function __construct(string $name)
    {
        $this->position = 0;
        $this->name = $name;

        Log::info("$name joined the game.");
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    public function addToPosition(int $move): void
    {
        $this->position += $move;
    }

    public function subtractFromPosition(int $move): void
    {
        $this->position -= $move;
    }
}

class Board
{

    private $maxPosition;
    private $pawns;
    private $dice;
    private $winner;
    private $turnsCounter;
    private $numberOfPawns;

    public function __construct($sides)
    {
        $this->pawns = [];
        $this->dice = new Dice($sides);
        $this->winner = null;
        $this->turnsCounter = 0;
    }

    public function __destruct()
    {
        Log::info("Board has been destroyed");
    }

    public function setNumberOfPawns(): void
    {
        $this->numberOfPawns = rand(3, 10);
    }

    public function getNumberOfPawns(): int
    {
        return $this->numberOfPawns;
    }


    public function addPawn(string $name): void
    {
        $pawn = new Pawn($name);
        $this->pawns[] = $pawn;
    }

    public function addPawns(int $numberOfPawns): void
    {
        for ($i = 0; $i < $numberOfPawns; $i++) {
            $name = readline("Give a pawn name:");
            $this->addPawn($name);
        }
    }


    public function setMaxPosition(): void
    {
        $this->maxPosition = readline("Give max position in the game:");
        if ($this->maxPosition < 1000 || $this->maxPosition > 5000) {
           throw new Exception ("Wrong number of fields");
        }
    }

    public function getMaxPosition():int
    {
        return $this->maxPosition;
    }

    public function getWinner(): Pawn
    {
        return $this->winner;
    }

    function performTurn(): void
    {
        $this->turnsCounter++;
        Log::info();
        Log::info("Turn " . $this->turnsCounter);

        foreach ($this->pawns as $pawn) {
            $rollResult = $this->dice->roll();

            if ($pawn->getPosition()%10==0)
            {
                Log::info ("Position is multiple of 10!");
                while ($rollResult==$this->dice->getNumberOfSides()){
                    Log::info("The maximum number was thrown away");
                    $bonus = $this->dice->roll();
                    $rollResult+=$bonus;
                }
            }


            if ($rollResult ==1 && $pawn->getPosition()%2==1)
            {
                Log::info("1 was thrown away and pawn is on odd field, so you have penalty throw");
                $penalty= $this->dice->roll();
                $pawn->subtractFromPosition($penalty);
                if ($pawn->getPosition()<0)
                {
                    $pawn->setPosition(0);
                }
            }

        else
        {
            $pawn->addToPosition($rollResult);
        }

            Log::info($pawn->getName() . " new position: " . $pawn->getPosition());

            if ($pawn->getPosition() >= $this->getMaxPosition()) {
                $this->winner = $pawn;
                throw new WinnerWasCalled();
            }
        }
    }

}


do {

    $dicesides = readline("Give number of dice sides :");


} while ($dicesides < 4 || $dicesides > 15 || $dicesides % 2 == 1);

$board = new Board($dicesides);

$board->setNumberOfPawns();
Log::info("There are " . $board->getNumberOfPawns() . " players in the game");
$board->addPawns($board->getNumberOfPawns());


try {
    $board->setMaxPosition();
} catch (Exception $exception) {
    Log::info();
    Log::info("Give correct number of fields (from 1000 to 5000)");
}


try {
    while (true) {
        $board->performTurn();
    }
} catch (WinnerWasCalled $exception) {
    Log::info();
    Log::info($board->getWinner()->getName() . " won.");
}
