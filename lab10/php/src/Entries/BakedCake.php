<?php

namespace PPO\Notebook\Entries;

use PPO\Notebook\Interfaces\NotebookEntry;
use PPO\Notebook\Traits\Slugger;

class BakedCake implements NotebookEntry {

    use Slugger;

    private $nameOfCake;

    public function __construct(string $nameOfCake) {
        $this->nameOfCake = $nameOfCake;
    }

    protected function getSlugBase(): string {
        return $this->nameOfCake;
    }

}
