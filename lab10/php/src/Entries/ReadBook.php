<?php

namespace PPO\Notebook\Entries;

use PPO\Notebook\Interfaces\NotebookEntry;
use PPO\Notebook\Traits\Slugger;

class ReadBook implements NotebookEntry {

    use Slugger;

    private $titleOfBook;
    private $author;

    public function __construct(string $titleOfBook, string $author) {
        $this->titleOfBook = $titleOfBook;
        $this->author=$author;
    }

    protected function getSlugBase(): string {
        return $this->titleOfBook . " " . $this->author;
    }

}
